// export typeDefs.
export type SimpleCountry = {
  _id: string;
  name: string;
  capital: string;
  population: number;
  flag: {
    svgFile: string;
  };
};

export type FullCountry = {
  _id: string;
  name: string;
  capital: string;
  population: number;
  area: number;
  flag: {
    svgFile: string;
  };
  topLevelDomains: {
    name: string;
  }[];
  location: {
    latitude: number;
    longitude: number;
  };
  distanceToOtherCountries: {
    countryName: string;
    distanceInKm: number;
  }[];
};
