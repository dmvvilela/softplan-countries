import { InMemoryCache, makeVar } from "@apollo/client";
import { FullCountry, SimpleCountry } from "./typeDefs";

// Initializes Apollo cache.
export const cache = new InMemoryCache({
  // typePolicies: {
  //   Query: {
  //     fields: {
  //       countries: {
  //         read() {
  //           return countriesVar();
  //         },
  //       },
  //       editedCountries: {
  //         read() {
  //           return editedCountriesVar();
  //         },
  //       },
  //     },
  //   },
  // },
});

// Initializes reactive variables.
export const countriesVar = makeVar<SimpleCountry[]>([]);
export const countryVar = makeVar<FullCountry | null>(null);
export const editIDVar = makeVar<string>("");
export const editedCountriesVar = makeVar<FullCountry[]>([]);
