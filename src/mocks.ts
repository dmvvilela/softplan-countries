export const mockSimpleCountry = {
  __typename: "Country",
  _id: "661",
  name: "Brazil",
  population: 206135893,
  capital: "Brasília",
  flag: {
    __typename: "Flag",
    svgFile: "https://restcountries.eu/data/bra.svg",
  },
};

export const mockFullCountry = {
  __typename: "Country",
  _id: "661",
  name: "Brazil",
  population: 206135893,
  capital: "Brasília",
  area: 8515767,
  flag: {
    __typename: "Flag",
    svgFile: "https://restcountries.eu/data/bra.svg",
  },
  topLevelDomains: [
    {
      __typename: "TopLevelDomain",
      name: ".br",
    },
  ],
  location: {
    __typename: "_Neo4jPoint",
    latitude: -10,
    longitude: -55,
  },
  distanceToOtherCountries: [
    {
      __typename: "DistanceToOtherCountry",
      countryName: "Bolivia (Plurinational State of)",
      distanceInKm: 1333.0445603821204,
    },
    {
      __typename: "DistanceToOtherCountry",
      countryName: "Paraguay",
      distanceInKm: 1481.9677422904354,
    },
    {
      __typename: "DistanceToOtherCountry",
      countryName: "Suriname",
      distanceInKm: 1562.413522321208,
    },
    {
      __typename: "DistanceToOtherCountry",
      countryName: "French Guiana",
      distanceInKm: 1574.1741073802189,
    },
    {
      __typename: "DistanceToOtherCountry",
      countryName: "Guyana",
      distanceInKm: 1727.7054803482656,
    },
  ],
};

export const mockDistances = {
  country1: [
    {
      __typename: "Country",
      name: "Bolivia (Plurinational State of)",
      location: {
        __typename: "_Neo4jPoint",
        latitude: -17,
        longitude: -65,
      },
    },
  ],
  country2: [
    {
      __typename: "Country",
      name: "Paraguay",
      location: {
        __typename: "_Neo4jPoint",
        latitude: -23,
        longitude: -58,
      },
    },
  ],
  country3: [
    {
      __typename: "Country",
      name: "Suriname",
      location: {
        __typename: "_Neo4jPoint",
        latitude: 4,
        longitude: -56,
      },
    },
  ],
  country4: [
    {
      __typename: "Country",
      name: "French Guiana",
      location: {
        __typename: "_Neo4jPoint",
        latitude: 4,
        longitude: -53,
      },
    },
  ],
  country5: [
    {
      __typename: "Country",
      name: "Guyana",
      location: {
        __typename: "_Neo4jPoint",
        latitude: 5,
        longitude: -59,
      },
    },
  ],
};
