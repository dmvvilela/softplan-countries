import React from "react";
import CountryList from "../components/CountryList";
import MenuBar from "../components/MenuBar";

const Home = () => {
  return (
    <div>
      <MenuBar />
      <CountryList />
    </div>
  );
};

export default Home;
