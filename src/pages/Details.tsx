import React from "react";
import CountryDetails from "../components/CountryDetails";
import MenuBar from "../components/MenuBar";

const Details = () => {
  return (
    <div>
      <MenuBar />
      <CountryDetails />
    </div>
  );
};

export default Details;
