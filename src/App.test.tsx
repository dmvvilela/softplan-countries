import React from "react";
import { render } from "@testing-library/react";
import App from "./App";
import { MockedProvider } from "@apollo/client/testing";

describe("when everything is OK", () => {
  test("should render the app without crashing", () => {
    render(
      <MockedProvider addTypename={false}>
        <App />
      </MockedProvider>
    );
    // screen.debug();
  });
});
