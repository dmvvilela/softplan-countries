import { gql } from "@apollo/client";

export const FETCH_COUNTRIES_LOCATION = gql`
  query Country(
    $name1: String!
    $name2: String!
    $name3: String!
    $name4: String!
    $name5: String!
  ) {
    country1: Country(name: $name1) {
      name
      location {
        latitude
        longitude
      }
    }
    country2: Country(name: $name2) {
      name
      location {
        latitude
        longitude
      }
    }
    country3: Country(name: $name3) {
      name
      location {
        latitude
        longitude
      }
    }
    country4: Country(name: $name4) {
      name
      location {
        latitude
        longitude
      }
    }
    country5: Country(name: $name5) {
      name
      location {
        latitude
        longitude
      }
    }
  }
`;
