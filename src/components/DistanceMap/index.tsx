import React from "react";
import { useQuery, useReactiveVar } from "@apollo/client";
import { countryVar } from "../../cache";
import {
  MapContainer,
  CircleMarker,
  Polyline,
  Popup,
  TileLayer,
} from "react-leaflet";
import { FETCH_COUNTRIES_LOCATION } from "./query";
import "leaflet/dist/leaflet.css";
import { Header } from "semantic-ui-react";

const DistanceMap = () => {
  const country = useReactiveVar(countryVar);
  const { loading, data, error } = useQuery(FETCH_COUNTRIES_LOCATION, {
    variables: {
      name1: country?.distanceToOtherCountries[0].countryName,
      name2: country?.distanceToOtherCountries[1].countryName,
      name3: country?.distanceToOtherCountries[2].countryName,
      name4: country?.distanceToOtherCountries[3].countryName,
      name5: country?.distanceToOtherCountries[4].countryName,
    },
    skip: !country,
  });

  if (!country) return <div></div>;
  if (loading) return <Header as="h2">Loading...</Header>;
  if (error) return <Header as="h2">An error ocurred...</Header>;

  const position: any = [country.location.latitude, country.location.longitude];

  return (
    <MapContainer
      center={position}
      zoom={4}
      scrollWheelZoom={false}
      style={{ height: 400, maxWidth: 400, margin: "20px auto" }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <CircleMarker
        center={position}
        pathOptions={{ fillColor: "blue" }}
        radius={15}
      >
        <Popup>{country.name}</Popup>
      </CircleMarker>
      {Object.keys(data).map((c) => {
        // console.log(c, data[c][0]);
        const ct = data[c][0];
        const pos: any = [ct.location.latitude, ct.location.longitude];
        return (
          <React.Fragment key={ct.name}>
            <Polyline
              pathOptions={{ color: "lime" }}
              positions={[position, pos]}
            />
            <CircleMarker
              center={pos}
              pathOptions={{ color: "red" }}
              radius={10}
            >
              <Popup>
                {ct.name}:{" "}
                {country.distanceToOtherCountries
                  .find((o) => o.countryName === ct.name)
                  ?.distanceInKm.toFixed(2)}
                km
              </Popup>
            </CircleMarker>
          </React.Fragment>
        );
      })}
    </MapContainer>
  );
};

export default DistanceMap;
