import React from "react";
import { screen, render, act } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { FETCH_COUNTRIES_LOCATION } from "./query";
import { MockedProvider } from "@apollo/client/testing";
import App from "../../App";
import { cache, countryVar } from "../../cache";
import { mockDistances, mockFullCountry } from "../../mocks";

const mocks = [
  {
    request: {
      query: FETCH_COUNTRIES_LOCATION,
      variables: {
        name1: "Bolivia (Plurinational State of)",
        name2: "Paraguay",
        name3: "Suriname",
        name4: "French Guiana",
        name5: "Guyana",
      },
    },
    result: {
      data: {
        mockDistances,
      },
    },
  },
];

describe("When everything is OK", () => {
  beforeEach(async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false} cache={cache}>
        <App />
      </MockedProvider>
    );
  });

  test("should render loading state", async () => {
    countryVar(mockFullCountry);
    await userEvent.click(screen.getByText("Details"));
    // const message = await screen.findByText(/Loading/);
    // expect(message).toBeInTheDocument();
  });

  test("should render the map correctly", async () => {
    countryVar(mockFullCountry);
    await act(async () => {
      await userEvent.click(screen.getByText("Details"));
      await new Promise((resolve) => setTimeout(resolve, 0));
    });
    // const message = await screen.findByText(/Distance from first 5 countries/);
    // expect(message).toBeInTheDocument();
  });
});
