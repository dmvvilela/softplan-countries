import { gql } from "@apollo/client";

export const FETCH_COUNTRY = gql`
  query Country($_id: String!) {
    Country(_id: $_id) {
      _id
      name
      population
      capital
      area
      flag {
        svgFile
      }
      topLevelDomains {
        name
      }
      location {
        latitude
        longitude
      }
      distanceToOtherCountries(first: 5) {
        countryName
        distanceInKm
      }
    }
  }
`;
