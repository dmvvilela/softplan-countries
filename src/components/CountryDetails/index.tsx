import React, { useState } from "react";
import { Header, Form, Image, Button, Message } from "semantic-ui-react";
import { useQuery, useReactiveVar } from "@apollo/client";
import { FETCH_COUNTRY } from "./query";
import {
  countriesVar,
  countryVar,
  editedCountriesVar,
  editIDVar,
} from "../../cache";
import DistanceMap from "../DistanceMap";
import { FullCountry } from "../../typeDefs";

const CountryDetails = () => {
  const editID = useReactiveVar(editIDVar);
  const { loading, data, error } = useQuery(FETCH_COUNTRY, {
    variables: { _id: editID },
    skip: !editID || !!editedCountriesVar().find((c) => c._id === editID),
  });
  const [values, setValues] = useState({
    name: "",
    capital: "",
    population: "",
    area: "",
    topLevelDomains: "",
  });
  const [submitState, setSubmitState] = useState("");

  // Caso já tenha sido editado, deve-se usar esses valores.
  let country = editedCountriesVar().find((c) => c._id === editID);

  // Caso a query tenha sido feita, atualiza a variável country reativa.
  if (!loading && !error && data) {
    country = data.Country[0];
    countryVar(country);
  }

  // Recupera os valores dos campos de edição (se já disponível) para o país.
  if (
    !values.name &&
    !values.capital &&
    !values.population &&
    !values.area &&
    !values.topLevelDomains &&
    country
  ) {
    setValues({
      name: country!.name,
      capital: country!.capital,
      population: country!.population.toString(),
      area: country!.area.toString(),
      topLevelDomains: country!.topLevelDomains[0].name,
    });
  }

  // Permite fazer um cache no client dos países editados.
  const updateEditedCountries = (country: FullCountry) => {
    const countries = editedCountriesVar();
    const index = countries.findIndex((c) => c._id === country._id);
    if (~index) {
      const countryEdit: any = {
        ...countries[index],
        name: values.name,
        capital: values.capital,
        area: values.area,
        population: values.population,
        topLevelDomains: [{ name: values.topLevelDomains }],
      };
      const countriesEdit = [...countries];
      countriesEdit[index] = countryEdit;
      editedCountriesVar([...countriesEdit]);
    } else {
      countries.push(country);
      editedCountriesVar([...countries]);
    }
  };

  // Realiza a edição de um país, atualizando as variáveis necessárias para cache.
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setSubmitState("");

    const countries = countriesVar();
    const country = countryVar();
    const index = countries.findIndex((c) => c._id === country?._id);
    if (~index) {
      let countryEdit: any = {
        ...country!,
        name: values.name,
        capital: values.capital,
        population: values.population,
      };
      const countriesEdit = [...countries];
      countriesEdit[index] = countryEdit;
      countriesVar([...countriesEdit]);

      countryEdit = {
        ...countryEdit,
        topLevelDomains: [{ name: values.topLevelDomains }],
      };
      countryVar({ ...countryEdit });
      updateEditedCountries({ ...countryEdit });
      setSubmitState("success");
    } else {
      setSubmitState("error");
    }
  };

  // Atualiza os campos na edição.
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <div>
      <Form
        onSubmit={onSubmit}
        success={submitState === "success"}
        error={submitState === "error"}
      >
        <Header as="h1" style={{ margin: 32 }}>
          Country Details
        </Header>
        {loading ? (
          <Header as="h2">Loading...</Header>
        ) : error ? (
          <Header as="h2">An error ocurred...</Header>
        ) : !country ? (
          <Header as="h2" style={{ margin: 32 }}>
            Please choose a country from list.
          </Header>
        ) : (
          <div>
            <div>
              <Image
                src={country!.flag.svgFile}
                centered
                size="medium"
                style={{ marginBottom: 32 }}
              />
            </div>
            <Form.Input
              label="Name"
              name="name"
              value={values.name}
              onChange={onChange}
              required
              style={{ maxWidth: 300 }}
            />
            <Form.Input
              label="Capital"
              name="capital"
              value={values.capital}
              onChange={onChange}
              style={{ maxWidth: 300 }}
            />
            <Form.Input
              label="Population"
              name="population"
              value={values.population}
              onChange={onChange}
              style={{ maxWidth: 300 }}
            />
            <Form.Input
              label="Area"
              name="area"
              value={values.area}
              onChange={onChange}
              style={{ maxWidth: 300 }}
            />
            <Form.Input
              label="Top Level Domains"
              name="topLevelDomains"
              value={values.topLevelDomains}
              onChange={onChange}
              style={{ maxWidth: 300, marginBottom: 8 }}
            />
            <Message
              success
              header="Success"
              content="Country successfully edited."
              style={{ maxWidth: 300, margin: "15px auto" }}
            />
            <Message
              error
              header="Error"
              content="Please try again in a few minutes."
              style={{ maxWidth: 300, margin: "15px auto" }}
            />
            <Button type="submit" primary>
              Confirm Edit
            </Button>
            <div>
              <Header as="h3" style={{ margin: 30 }}>
                Distance from first 5 countries
              </Header>
              <DistanceMap />
            </div>
          </div>
        )}
      </Form>
    </div>
  );
};

export default CountryDetails;
