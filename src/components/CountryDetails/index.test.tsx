import React from "react";
import { screen, render, act } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { FETCH_COUNTRY } from "./query";
import { MockedProvider } from "@apollo/client/testing";
import App from "../../App";
import { cache, editIDVar } from "../../cache";
import { mockFullCountry } from "../../mocks";

const mocks = [
  {
    request: {
      query: FETCH_COUNTRY,
      variables: {
        _id: "661",
      },
    },
    result: {
      data: {
        Country: [mockFullCountry],
      },
    },
  },
];

describe("When everything is OK", () => {
  beforeEach(async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false} cache={cache}>
        <App />
      </MockedProvider>
    );
  });

  test("should render error message from not choosing country", async () => {
    await userEvent.click(screen.getByText("Details"));
    const message = await screen.findByText(
      /Please choose a country from list/
    );
    expect(message).toBeInTheDocument();
  });

  test("should render loading state", async () => {
    editIDVar("61");
    await userEvent.click(screen.getByText("Details"));
    const message = await screen.findByText(/Loading/);
    expect(message).toBeInTheDocument();
    screen.debug();
  });

  test("should render the country details", async () => {
    editIDVar("61");
    await act(async () => {
      await userEvent.click(screen.getByText("Details"));
      await new Promise((resolve) => setTimeout(resolve, 0));
    });
    const message = await screen.findByText(/Country Details/);
    expect(message).toBeInTheDocument();
    // const name = await screen.findByText(/Name/);
    // expect(name).toBeInTheDocument();
    // const capital = await screen.findByText(/Capital/);
    // expect(capital).toBeInTheDocument();
    // const population = await screen.findByText(/Population/);
    // expect(population).toBeInTheDocument();
    // const area = await screen.findByText(/Area/);
    // expect(area).toBeInTheDocument();
    // const topLevelDomains = await screen.findByText(/Top Level Domains/);
    // expect(topLevelDomains).toBeInTheDocument();
    // const textbox = screen.getAllByRole("textbox");
    // expect(textbox).toHaveLength(5);
  });
});
