import React from "react";
import { screen, render, act } from "@testing-library/react";
import { FETCH_COUNTRY_LIST } from "./query";
import { MockedProvider } from "@apollo/client/testing";
import App from "../../App";
import { cache } from "../../cache";
import { mockSimpleCountry } from "../../mocks";

const mocks = [
  {
    request: {
      query: FETCH_COUNTRY_LIST,
    },
    result: {
      data: {
        Country: [mockSimpleCountry],
      },
    },
  },
];

describe("When everything is OK", () => {
  beforeEach(async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false} cache={cache}>
        <App />
      </MockedProvider>
    );
  });

  test("should render loading state", async () => {
    const message = await screen.findByText(/Loading/);
    expect(message).toBeInTheDocument();
  });

  test("should render the search input", async () => {
    await act(async () => {
      await new Promise((resolve) => setTimeout(resolve, 0));
    });
    const message = await screen.findByText(/Country List/);
    expect(message).toBeInTheDocument();
    const search = screen.getByPlaceholderText(/Country or capital.../);
    expect(search).toBeInTheDocument();
    // const country = await screen.findByText(/Brazil/);
    // expect(country).toBeInTheDocument();
    // screen.debug();
  });

  test("should render the country list", async () => {
    await act(async () => {
      await new Promise((resolve) => setTimeout(resolve, 0));
    });
    // const country = await screen.findByText(/Brazil/);
    // expect(country).toBeInTheDocument();
  });
});
