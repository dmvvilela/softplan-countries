import React, { useState } from "react";
import { FETCH_COUNTRY_LIST } from "./query";
import {
  Header,
  Grid,
  Input,
  InputOnChangeData,
  Pagination,
  PaginationProps,
} from "semantic-ui-react";
import CountryCard from "../CountryCard";
import { useQuery, useReactiveVar } from "@apollo/client";
import { countriesVar } from "../../cache";
import { SimpleCountry } from "../../typeDefs";

const CountryList = () => {
  const { loading, data, error } = useQuery(FETCH_COUNTRY_LIST);
  const countries = useReactiveVar(countriesVar);
  const [filtered, setFiltered] = useState<string>("");
  const [page, setPage] = useState<number>(1);

  if (!loading && !error && data) {
    if (!countriesVar().length) {
      countriesVar([...data.Country]);
    }
  }

  const handleSearch = (
    e: React.ChangeEvent<HTMLInputElement>,
    data: InputOnChangeData
  ) => {
    setFiltered(data.value.toLowerCase());
    setPage(1);
  };

  const handleFilter = (country: SimpleCountry) =>
    country.name.toLowerCase().includes(filtered) ||
    country.capital.toLowerCase().includes(filtered);

  const handlePageChange = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    data: PaginationProps
  ) => setPage(data.activePage as number);

  return (
    <div style={{ margin: 16, marginBottom: 72 }}>
      <Grid centered>
        <Grid.Row centered>
          <Header as="h1" style={{ margin: 16 }}>
            Country List
          </Header>
        </Grid.Row>
        <Grid.Row>
          <Input
            icon="search"
            placeholder="Country or capital..."
            onChange={handleSearch}
          />
        </Grid.Row>
        {loading ? (
          <Header as="h2">Loading...</Header>
        ) : error ? (
          <Header as="h2">An error ocurred...</Header>
        ) : (
          countries
            .filter(handleFilter)
            .slice((page - 1) * 9, page * 9)
            .map((country: SimpleCountry) => (
              <Grid.Column
                key={country.name}
                mobile={16}
                tablet={9}
                computer={5}
              >
                <CountryCard country={country} />
              </Grid.Column>
            ))
        )}
        {/* </Grid.Row> */}
      </Grid>
      {countries.length && (
        <Pagination
          activePage={page}
          totalPages={Math.ceil(countries.length / 9)}
          onPageChange={handlePageChange}
          style={{ margin: 48 }}
        />
      )}
    </div>
  );
};

export default CountryList;
