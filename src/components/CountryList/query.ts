import { gql } from "@apollo/client";

export const FETCH_COUNTRY_LIST = gql`
  query CountryList {
    Country {
      _id
      name
      population
      capital
      flag {
        svgFile
      }
    }
  }
`;
