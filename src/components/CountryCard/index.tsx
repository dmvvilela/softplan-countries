import React from "react";
import { Card, CardProps, Icon, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { editIDVar } from "../../cache";
import { SimpleCountry } from "../../typeDefs";

const CountryCard = (props: { country: SimpleCountry }) => {
  const { country } = props;

  const setEditID = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    data: CardProps
  ) => {
    editIDVar(country._id);
  };

  return (
    <Card
      style={{ height: "100%" }}
      fluid
      as={Link}
      to="/details"
      onClick={setEditID}
    >
      <Image src={country.flag.svgFile} wrapped ui={false} />
      <Card.Content>
        <Card.Header>{country.name}</Card.Header>
        <Card.Meta>{country.capital}</Card.Meta>
      </Card.Content>
      <Card.Content extra>
        <Icon name="user" />
        {country.population}
      </Card.Content>
    </Card>
  );
};

export default CountryCard;
