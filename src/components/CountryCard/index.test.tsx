import React from "react";
import { screen, render } from "@testing-library/react";
import CountryCard from ".";
import { SimpleCountry } from "../../typeDefs";
import { BrowserRouter as Router } from "react-router-dom";

const mock: SimpleCountry = {
  _id: "61",
  name: "Brazil",
  capital: "Brasília",
  population: 206135893,
  flag: {
    svgFile: "https://restcountries.eu/data/bra.svg",
  },
};

describe("When everything is OK", () => {
  test("should render country card", async () => {
    render(
      <Router>
        <CountryCard country={mock} />
      </Router>
    );
    const name = await screen.findByText("Brazil");
    expect(name).toBeInTheDocument();
    const capital = await screen.findByText("Brasília");
    expect(capital).toBeInTheDocument();
    const population = await screen.findByText("206135893");
    expect(population).toBeInTheDocument();
  });
});
