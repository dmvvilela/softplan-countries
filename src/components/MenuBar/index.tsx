import React from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

const MenuBar = () => {
  const pathname = window.location.pathname;
  const path = pathname === "/" ? "home" : pathname.substr(1);
  const activeItem = path;

  return (
    <div>
      <Menu pointing secondary color="teal">
        <Menu.Item
          name="home"
          active={activeItem === "home"}
          as={Link}
          to="/"
        />
        <Menu.Item
          name="details"
          active={activeItem === "details"}
          as={Link}
          to="/details"
        />
      </Menu>
    </div>
  );
};

export default MenuBar;
