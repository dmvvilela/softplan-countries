import React from "react";
import { screen, render } from "@testing-library/react";
import MenuBar from ".";
import { BrowserRouter as Router } from "react-router-dom";

describe("When everything is OK", () => {
  test("should render menu bar", async () => {
    render(
      <Router>
        <MenuBar />
      </Router>
    );
    const home = await screen.findByText("Home");
    expect(home).toBeInTheDocument();
    const details = await screen.findByText("Details");
    expect(details).toBeInTheDocument();
  });
});
