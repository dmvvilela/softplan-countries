import {
  countryVar,
  countriesVar,
  editIDVar,
  editedCountriesVar,
} from "./cache";
import { mockFullCountry, mockSimpleCountry } from "./mocks";

describe("Testing reactive variables", () => {
  test("should have initial values", () => {
    expect(countryVar()).toBe(null);
    expect(editIDVar()).toBe("");
    expect(countriesVar()).toStrictEqual([]);
    expect(editedCountriesVar()).toStrictEqual([]);
  });

  test("should update value on call", () => {
    countryVar(mockFullCountry);
    expect(countryVar()).toBe(mockFullCountry);

    editIDVar("61");
    expect(editIDVar()).toBe("61");

    countriesVar([mockSimpleCountry]);
    expect(countriesVar()).toStrictEqual([mockSimpleCountry]);

    editedCountriesVar([mockFullCountry]);
    expect(editedCountriesVar()).toStrictEqual([mockFullCountry]);
  });
});
