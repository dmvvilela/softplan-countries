import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import "./App.css";
import Home from "./pages/Home";
import Details from "./pages/Details";
import { Container } from "semantic-ui-react";

function App() {
  return (
    <div className="App">
      <Container>
        <Router>
          <Route exact path="/" component={Home} />
          <Route exact path="/details" component={Details} />
        </Router>
      </Container>
    </div>
  );
}

export default App;
