# Configuração ao ambiente

O projeto foi criado com o `create-react-app` com o template de typescript, portanto os scripts padrão foram utilizados, assim como a checagem estática do typescript, não necessitando assim do uso de proptypes do react ou algum padrão de linting extra.

Utilize o comando `yarn` para instalar as dependências do projeto antes de tudo.

Pode-se iniciar o projeto localmente com `yarn start`, realizar um build para produção com `yarn build` e testar o projeto com `yarn test`.

Foi adicionado também um comando para verificar o coverage de testes. Utilize o comando `yarn coverage`.

# Estrutura do código

O projeto está dividido de acordo com a responsabilidade de cada pasta/arquivo.
Uma pasta "components" para os componentes do projeto, onde cada um possui seus próprios testes unitários e sua query graphQL em sua pasta individual.

A pasta "pages" referencia as rotas para o react-router com o componente pai de cada uma.

Na pasta raíz temos os arquivos de tipos (pois é utilizado typescript) e cache para o Apollo.

O projeto utiliza o @apollo/client versão 3 para gerenciamento do estado local e das queries graphQL.

A idéia do projeto é utilizar o princípio do KISS, ou seja, tudo simplificado ao máximo, desde que mantenha a "elegância" do código, a manutenabilidade e a inteligibilidade.

# UI / UX

Foi utilizado a biblioteca semantic-ui (implementação para react) para a interface. É bem simples e intuitiva, com um design elegante e responsivo.

A lista de países possui paginação e também existe um menu em todas as páginas para navegação via react-router.

Também pode-se pesquisar por país ou capital na caixa de texto do componente.

Para o mapa foi utilizado o leaflet por ser gratuito. Também sua versão react.

# Gerenciamento de estado

Existem diversas formas de fazer isso. Optou-se, como diferencial, utilizar o local state management do apollo ao invés de usar o redux.

Para isso também há diversas formas de realizar o gerencimento. Portanto usou-se uma feature mais nova (do apollo 3) chamada reactive variables.

Elas simplificam o código e o gerenciamento de estado pela reatividade mantendo assim o princípio do KISS mencionado acima.

Fora isso, utilizou-se react hooks para o estado local de alguns componentes (useState).

# Deploy

Para o deploy utilizou-se o netlify, pois além de ser gratuito, implementa automaticamente o deploy quando é feito um push das modificações no branch master.

[https://competent-lewin-95cd29.netlify.app/](https://competent-lewin-95cd29.netlify.app/)

# Pipeline de testes

Para o melhor aproveitamento do netlify e mantendo o projeto simples, foi definido que o pipeline de CI/CD do Gitlab seria utilizado da seguinte forma:

build -> test -> deploy

Os stages de build e deploy são simples, apenas utiliza-se os comandos do yarn definidos no script.

Para o deploy, com o branch master protegido por default, realiza-se o desenvolvimento no branch dev, e sempre que subir modificações nesse branch, caso os stages anteriores tenham sido realizados com sucesso, o sistema irá automaticamente fazer um merge ao branch master e então o deploy será feito pelo netlify.

Assim, garante-se que apenas as modificações que não quebrem o projeto terão seu deploy de fato realizado.

# Gitlab

O repositório se encontra no Gitlab através do link: [https://gitlab.com/dmvvilela/softplan-countries](https://gitlab.com/dmvvilela/softplan-countries)

# Utilização

O app está bem intuitivo. Uma lista de países aparece na Home, ao clicar em qualquer um o usuário poderá editá-lo (client only).

Na lista de detalhes também é apresentado um mapa (via leaflet) com os 5 países mais próximos. Ao clicar em cada marcador do país é possível também verificar seu nome e a distância do páis detalhado.
